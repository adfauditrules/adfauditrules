package org.adfemg.audits.utils;

import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class XmlUtils {
    /**
     * Checks if a nodeName exist in a NodeList.
     *
     * @param nodeList the NodeList to check.
     * @param nodeName the name to look up.
     * @return true if found, false if not.
     */
    public static boolean doesNodeExist(NodeList nodeList, String nodeName) {
        for (int nodes = 0; nodes < nodeList.getLength(); nodes++) {
            Node node = nodeList.item(nodes);
            if (node != null && nodeName.equals(node.getNodeName())) {
                return true;
            }
        }
        return false;
    }

    /**
     * Looks for a node in a NodeList, returns the first one if any is found.
     * Returns null if none is found.
     *
     * @param nodeList the NodeList to check.
     * @param nodeName the nodeName to look up.
     * @return Node if found, null if not.
     */
    public static Node findFirstNodeInList(NodeList nodeList, String nodeName) {
        for (int nodes = 0; nodes < nodeList.getLength(); nodes++) {
            Node node = nodeList.item(nodes);
            if (node != null && nodeName.equals(node.getNodeName())) {
                return node;
            }
        }
        return null;
    }

    /**
     * Looks for a attribute in a NamedNodeMap, returns the first one if any is found.
     * Returns null if none is found.
     *
     * @param nodeMap
     * @param attributeName
     * @return the first Node if any is found, null if none is found.
     */
    public static Node findFirstAttributeInList(NamedNodeMap nodeMap, String attributeName) {
        for (int attr = 0; attr < nodeMap.getLength(); attr++) {
            Node attribute = nodeMap.item(attr);
            if (attribute != null && attributeName.equals(attribute.getNodeName())) {
                return attribute;
            }
        }
        return null;
    }

    /**
     * Checks if a nodeName exist in a NamedNodeMap.
     * @param nodeMap
     * @param attributeName
     * @return true if nodeName exist, false if it does not.
     */
    public static boolean doesAttributExist(NamedNodeMap nodeMap, String attributeName) {
        for (int attr = 0; attr < nodeMap.getLength(); attr++) {
            Node attribute = nodeMap.item(attr);
            if (attribute != null && attributeName.equals(attribute.getNodeName())) {
                return true;
            }
        }
        return false;
    }

    /**
     * Returns the name of the document (root) element if it exists, or <code>null</code>
     * if none can be found. This can be used to prevent NullPointerExceptions when
     * requesting document.getDocumentElement().getNodeName() directly.
     * @param document XML document
     * @return name of the document root element or <code>null</code> if input document
     * is <code>null</code> or the document does not have a document element (eg empty doc)
     */
    public static String docElementName(Document document) {
        return document != null && document.getDocumentElement() != null ? document.getDocumentElement().getNodeName() :
               null;
    }
}
