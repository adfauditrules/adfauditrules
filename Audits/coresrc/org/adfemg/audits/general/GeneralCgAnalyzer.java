package org.adfemg.audits.general;

import oracle.jdeveloper.audit.analyzer.AuditContext;
import oracle.jdeveloper.audit.analyzer.Rule;
import oracle.jdeveloper.audit.extension.ExtensionResource;

import org.adfemg.audits.base.XmlAnalyzer;
import org.adfemg.audits.utils.XmlUtils;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;


/**
 * Analyzer class for the General Code Guidelines.
 * Implemented ruls for web.xml and weblogic.xml file.
 */
public class GeneralCgAnalyzer extends XmlAnalyzer {
    /**
     * [ADFcg1-01003] – Add a default activity ID to the welcome-file - always add a default activity ID
     * from adfc-config.xml file to your web.xml <welcome-file> entry. This becomes the activity the
     * application navigates to when a timeout occurs in a popup or the user opens a new window with Ctrl-n in
     * their browser while in a bounded task flow which isn't a valid starting point for the new window
     */
    @ExtensionResource("org.adfemg.audits.general.default-activity")
    private Rule DEFAULT_ACTIVITY;

    /**
     * [ADFcg1-01005] - url.rewriting.enabled should be false - If enabled, the feature would cause security
     * vulnerability because the session identifier would be exposed in certain scenarios
     */
    @ExtensionResource("org.adfemg.audits.general.url-rewriting")
    private Rule URL_REWRITING;

    public GeneralCgAnalyzer() {
        super(null);
    }

    @Override
    protected boolean isAuditable(Document document) {
        String rootElemName = XmlUtils.docElementName(document);
        return "web-app".equals(rootElemName) || "weblogic-web-app".equals(rootElemName);
    }

    public void exit(AuditContext context, Element element) {
        //check if the welcome-file-list exist.
        //if it does, the child element welcome-file is required by JDev.
        if ("web-app".equals(element.getNodeName())) {
            NodeList nodeList = element.getChildNodes();
            if (!XmlUtils.doesNodeExist(nodeList, "welcome-file-list")) {
                context.report(DEFAULT_ACTIVITY);
            }
        }
        //Check the value of element url-rewriting, this should be false.
        if ("url-rewriting-enabled".equals(element.getNodeName())) {
            if (!"false".equals(element.getTextContent())) {
                context.report(URL_REWRITING);
            }
        }
        //check if the node url-rewriting exists.
        if ("weblogic-web-app".equals(element.getNodeName())) {
            NodeList nodeList = element.getChildNodes();
            if (!XmlUtils.doesNodeExist(nodeList, "url-rewriting-enabled")) {
                context.report(URL_REWRITING);
            }
        }
    }
}

