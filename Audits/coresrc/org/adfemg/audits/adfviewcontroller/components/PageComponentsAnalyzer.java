package org.adfemg.audits.adfviewcontroller.components;

import oracle.jdeveloper.audit.analyzer.Analyzer;
import oracle.jdeveloper.audit.analyzer.AuditContext;
import oracle.jdeveloper.audit.analyzer.Rule;
import oracle.jdeveloper.audit.extension.ExtensionResource;

import org.adfemg.audits.utils.AuditConstants;
import org.adfemg.audits.utils.AuditUtils;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;


/**
 * Analyzer class for PageComponents.
 */

public class PageComponentsAnalyzer extends Analyzer {
    /**
     * ADFcg1-03040 Use rendered=false over visible=false if possible - If you wish to hide a
     * component use rendered=false over visible=false when possible as the first results in the server skipping
     * the processing of the component and sending it over the wire to the browser, while the 2nd requires the
     * server to process the component, send it over the wire, the browser to process it then hide it.
     */
    @ExtensionResource("org.adfemg.audits.adfviewcontroller.components.visible-false-2")
    private Rule VISIBLE_FALSE_2;

    /**
     * ADFcg1-03041 Consider that visible=false presents a security risk for components - As
     * components with visible=false are still sent to the client then hidden, it's easy for hackers to see them in
     * the browser DOM, manipulate them and if they're submittable components send them on the next
     * request. Instead either use rendered=false so the component isn't sent to the browser in the first place, or
     * back the submittable component by a validation rule to ensure it can't be submitted when the component
     * is meant to be hidden.
     */
    @ExtensionResource("org.adfemg.audits.adfviewcontroller.components.visible-false")
    private Rule VISIBLE_FALSE;

    /**
     * [ADFcg1-03039] - Avoid unnecessary use of clientComponent=true - Setting clientComponent=true
     * on any component results in a larger DOM structure on the browser which will have a small impact hit
     * both terms of speed and memory usage. Only use clientComponent=true when necessary.
     */
    @ExtensionResource("org.adfemg.audits.adfviewcontroller.components.client-component-true")
    private Rule CLIENT_COMPONENT_TRUE;

    /**
     * [ADFcg1-03074] - Use AFStretchWidth / AFAuxiliaryStretchWidth styles rather than absolute 100%
     * width styling - These pre-existing styles take into account the overall geometry management of the various
     * ADF layout containers in different browsers and will give more reliable results.
     */
    @ExtensionResource("org.adfemg.audits.adfviewcontroller.skinning.width-100")
    private Rule WIDTH_100;

    /**
     * [ADFcg1-03075] - Do not use percentage height styles - Heights expressed as a percentage are not
     * reliable and behavior differs between browsers. Use absolute values for height if height must be specified
     * at all
     */
    @ExtensionResource("org.adfemg.audits.adfviewcontroller.skinning.height-perc")
    private Rule HEIGHT_PERC;

    /**
     * ADFcg1-03050 Avoid inline page/fragment JavaScript and CSS. Avoid placing custom JavaScript and CSS
     * in your page and page fragments as these must be loaded by the browser each time the page/fragment is accessed.
     * Instead place the JavaScript and CSS respectively in separate .JS and .CSS files so they can be cached by the browser.
     */
    @ExtensionResource("org.adfemg.audits.adfviewcontroller.skinning.inline-js")
    private Rule INLINE_JS;

    /**
     * [ADFcg1-03038] - Abbreviate "container" component ID lengths to 4 characters.
     * The number 4 here is arbitrary. However for container component, that is components that contain other components,
     * as their ID gets repeated multiple times in all the child component IDs, the smaller the ID you use here
     * the smaller and more efficient the page will be.
     */
    @ExtensionResource("org.adfemg.audits.adfviewcontroller.components.cont_id_length")
    private Rule CONT_ID_LENGTH;

    /**
     * [ADFcg1-03037] Avoid long component ID lengths. Avoid long component ID names as this has a performance hit.
     */
    @ExtensionResource("org.adfemg.audits.adfviewcontroller.components.id_length")
    private Rule ID_LENGTH;

    /**
     * [ADFcg1-03065] Limit the number of page regions. Limit the number of ADF regions you render in a page to 10.
     */
    @ExtensionResource("org.adfemg.audits.adfviewcontroller.components.limit_regions")
    private Rule LIMIT_REGION;

    /**
     * Check if we're in a page or a page fragment file.
     * @param context the AuditContext.
     * @param document the document.
     */
    public void enter(AuditContext context, Document document) {
        String fileName = context.getUrl().getFile();
        if (!fileName.matches(".*.jsf") && !fileName.matches(".*.jspx") && !fileName.matches(".*.jsff")) {
            setEnabled(false);
        }
    }

    public void exit(AuditContext context, Document document) {
        NodeList nodes = document.getElementsByTagNameNS(AuditConstants.NS_AF, "region");
        if (nodes.getLength() > AuditConstants.MAX_REGIONS_IN_PAGE) {
            context.report(LIMIT_REGION);
        }
    }

    /**
     * Check if the elements to avoid are included in the page.
     */
    public void exit(AuditContext context, Element element) {
        if ("resource".equals(element.getLocalName())) {
            if ("javascript".equals(element.getAttribute("type")) && !element.hasAttribute("source") &&
                element.getTextContent() != null && element.getTextContent().contains("function")) {
                //type resource is javascript, geen source attr aanwezig, text bevat function.
                //TODO: wat voor text checken voor JS?!
                context.report(INLINE_JS);
            }
        }
        if (AuditUtils.isGroupComponent(element.getLocalName())) {
            if (element.getAttribute("id").length() > AuditConstants.MAX_CONTAINER_LENGTH) {
                context.report(CONT_ID_LENGTH);
            }
        }
    }

    public void exit(AuditContext context, Attr attr) {
        //Visible checks
        if ("visible".equals(attr.getName()) && "false".equals(attr.getValue())) {
            context.report(VISIBLE_FALSE);
            context.report(VISIBLE_FALSE_2);
        }
        if ("clientComponent".equals(attr.getName()) && "true".equals(attr.getValue())) {
            context.report(CLIENT_COMPONENT_TRUE);
        }
        //Skinning checks width/height:
        if (attr.getName().toUpperCase().contains("width".toUpperCase()) && attr.getValue().contains("100%")) {
            context.report(WIDTH_100);
        }
        if (attr.getName().toUpperCase().contains("height".toUpperCase()) && attr.getValue().contains("%")) {
            context.report(HEIGHT_PERC);
        }
        //max id if element is not a groupComponent.
        if ("id".equals(attr.getName()) && attr.getValue() != null &&
            attr.getValue().length() > AuditConstants.MAX_ID_LENGTH &&
            !AuditUtils.isGroupComponent(attr.getOwnerElement().getLocalName())) {
            context.report(ID_LENGTH);
        }
    }
}

