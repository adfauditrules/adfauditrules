package org.adfemg.audits.java;

import oracle.javatools.parser.java.v2.CallerContext;
import oracle.javatools.parser.java.v2.model.JavaType;
import oracle.javatools.parser.java.v2.model.SourceFieldDeclaration;
import oracle.javatools.parser.java.v2.model.statement.SourceExpressionStatement;

import oracle.jdeveloper.audit.analyzer.Analyzer;
import oracle.jdeveloper.audit.analyzer.AuditContext;
import oracle.jdeveloper.audit.analyzer.Rule;
import oracle.jdeveloper.audit.extension.ExtensionResource;

/**
 * Analyzer class for Java.
 */
public class JavaAnalyzer extends Analyzer {
    /**
     * [ADFcg1-01028] - Don't use System.out.* or System.err.* - Do not use functions like
     * System.out.println.for debugging and do not leave this unnecessarily in your code.
     */
    @ExtensionResource("org.adfemg.audits.java.system-out-usage")
    private Rule SYSTEM_OUT_USAGE;

    /**
     * [ADFcg1-01018] - Don't use System.exit() - Executing System.exit() in a running ADF application will
     * force the JVM container to quit. Do not use this.
     */
    @ExtensionResource("org.adfemg.audits.java.system-exit-usage")
    private Rule SYSTEM_EXIT_USAGE;

    @ExtensionResource("org.adfemg.audits.java.comp-ref")
    private Rule COMPONENT_REFERENCE;

    /**
     * Check if the ExpressionStmt contains System.out. System.err. or System.exit
     * @param context the AuditContext
     * @param stmt the (java) statement
     */
    public void exit(AuditContext context, SourceExpressionStatement stmt) {
        String methodText = stmt.getText();
        if (methodText.contains("System.out.") || methodText.contains("System.err.")) {
            context.report(SYSTEM_OUT_USAGE);
        }
        if (methodText.contains("System.exit")) {
            context.report(SYSTEM_EXIT_USAGE);
        }
    }

    public void exit(AuditContext context, SourceFieldDeclaration field) {
        if (field == null) {
            return;
        }
        JavaType javaType = field.getResolvedType();
        if (javaType == null) {
            return;
        }
        CallerContext callerCtx = CallerContext.createContext(field);
        if (callerCtx == null) {
            return;
        }
        JavaType uiCompType = callerCtx.resolveTypeName("javax.faces.component.UIComponent");
        if (javaType.isSubtypeOf(uiCompType)) {
            context.report(COMPONENT_REFERENCE, field);
        }
    }
}
