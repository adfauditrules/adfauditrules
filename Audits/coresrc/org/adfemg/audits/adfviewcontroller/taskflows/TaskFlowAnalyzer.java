package org.adfemg.audits.adfviewcontroller.taskflows;

import oracle.ide.model.Workspaces;

import oracle.jdeveloper.audit.analyzer.AuditContext;
import oracle.jdeveloper.audit.analyzer.Rule;
import oracle.jdeveloper.audit.extension.ExtensionResource;

import org.adfemg.audits.base.XmlAnalyzer;
import org.adfemg.audits.utils.AuditUtils;
import org.adfemg.audits.utils.XmlUtils;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;


/**
 * Analyzer class for TaskFlow files.
 */

public class TaskFlowAnalyzer extends XmlAnalyzer {
    /**
     * [ADFcg1-03097] Use isolated data control scope only when required.
     * Having an isolated data control scope can be very useful for those specific scenarios where you need separate transactions.
     * Be aware of the cost in terms of resources and connections when using this facility.
     */
    @ExtensionResource("org.adfemg.audits.adfviewcontroller.taskflows.tf-dc-scope")
    private Rule TF_DC_SCOPE;

    /**
     * [ADFcg1-03085] � Define task flow exception handlers.Every task flow should include an exception
     * handler mechanism.
     */
    @ExtensionResource("org.adfemg.audits.adfviewcontroller.taskflows.tf-exception-handler")
    private Rule TF_EXCEPTION_HANDLER;

    /**
     * [ADFcg1-03082] - Define single task flow return commit/rollback instances.
     */
    @ExtensionResource("org.adfemg.audits.adfviewcontroller.taskflows.tf-commit-rollback")
    private Rule TF_COMMIT_ROLLBACK;

    /**
     * [ADFcg1-03083] - Use of diagram annotations or <description> attribute to describe purpose
     * of the task flow.
     */
    @ExtensionResource("org.adfemg.audits.adfviewcontroller.taskflows.tf-description-exist")
    private Rule TF_DESCRIPTION;

    /**
     * If the managed-property node exist, the "property-class" should be set!
     */
    @ExtensionResource("org.adfemg.audits.adfviewcontroller.taskflows.tf-mb-property-hasclass")
    private Rule TF_MB_PROPERTY;


    /**
     * boolean flag which gets initalized to false on entry of every task flow file in enter method.
     * It is set to true if task flow uses exception handler.
     */    
    private boolean _exceptionHandlerExists;
    private AuditContext.Key excHandlerExists;

    /**
     * boolean flag which gets true if task flow has description set.
     */    
    private AuditContext.Key descrExists;    

    /**
     * boolean flag which gets true if task flow is based on taskflow template and template conisit of exception handler.
     */    
    private AuditContext.Key hasExceptionHandler;

    public TaskFlowAnalyzer() {
        super("adfc-config");
    }

    @Override
    protected boolean isAuditable(Document document) {
        if (!super.isAuditable(document)) {
            return false;
        }
        Element docElem = document != null ? document.getDocumentElement() : null;
        NodeList children = docElem != null ? docElem.getChildNodes() : null;
        
        //task-flow-definition does not need to be the first element in the nodeList.
        //So we need to loop through all children to find out wether we are a taskflow or not. 
        boolean isTF = false;
        for (int nodes = 0; nodes < children.getLength(); nodes++) {
            Node node = children.item(nodes);
            if("task-flow-definition".equals(node.getNodeName())) {
                isTF = true;
                break;
            }                                
        }    
        return isTF;
    }

    public void enter (AuditContext context, Workspaces workspaces) {
        hasExceptionHandler = context.key("hasExceptionHandler");
        descrExists = context.key("descrExists");      
        excHandlerExists = context.key("excHandlerExists");              
    }

    /**
     * Check if we're in a adfc-config file and in a task-flow-definition file.
     * @param context the AuditContext.
     * @param document the document.
     */
    public void enter(AuditContext context, Document document) {
        super.enter(context, document);        
        context.setAttribute(excHandlerExists, false);             
        context.setAttribute(hasExceptionHandler, false);     
        context.setAttribute(descrExists, false);             
    }

    /**
     * Check if the element is isolated and if the parent is data-control-scope.
     * Check if the task flow includes exception handling mechanism.
     */
    public void exit(AuditContext context, Element element) {
        if ("isolated".equals(element.getNodeName()) &&
            "data-control-scope".equals(element.getParentNode().getNodeName())) {
            context.report(TF_DC_SCOPE);
        }
        //Below three If statements checks for exception handlers in the task flow defination file.
        if (("exception-handler".equals(element.getNodeName())) && element.getTextContent() != null) {            
                context.setAttribute(excHandlerExists, true);            
        }

        // Following condition is checked if taskflow is based on taskflow template. If yes, it calls AuditUtils.handlerExistInTFTemplate
        // method and returns true if TaskFlow template has exception handler defined.
        if ("template-reference".equals(element.getNodeName())) {
            NodeList nodeList = element.getChildNodes();
            Node node = XmlUtils.findFirstNodeInList(nodeList, "document");
            if (node != null) {
                boolean tmpVal =
                    AuditUtils.handlerExistInTFTemplate(node.getLastChild().getTextContent(),
                                                        context.getUrl().getPath());
                context.setAttribute(hasExceptionHandler, tmpVal);
            }
        }

        //If the managed-property node exist, the "property-class" should be set!
        if ("managed-property".equals(element.getNodeName())) {
            boolean propFound = false;
            NodeList childNodesList = element.getChildNodes();
            for (int nodes = 0; nodes < childNodesList.getLength(); nodes++) {
                Node node = childNodesList.item(nodes);
                if("property-class".equals(node.getNodeName())) {
                    propFound = true;
                }                                
            }    
            if (!propFound) {
                context.report(TF_MB_PROPERTY);   
            }
        }

        //Check if description exist for taskflow.
        if ("description".equals(element.getNodeName())) {
            if (element.getLastChild().getNodeValue() != null) {
                context.setAttribute(descrExists, true);                 
            }
        }

        if ("adfc-config".equals(element.getNodeName())) {
            if (!(Boolean)context.getAttribute(excHandlerExists) && !(Boolean)context.getAttribute(hasExceptionHandler)) {
                context.report(TF_EXCEPTION_HANDLER);
            }
            if (!(Boolean)context.getAttribute(descrExists)) {
                context.report(TF_DESCRIPTION);
            }
        }

        //If taskflow requires a task flow return commit or rollback, call checkForReturnActivities.
        //If it returns false report voilation.
        if ("new-transaction".equals(element.getNodeName()) || "requires-transaction".equals(element.getNodeName())) {
            if (!checkForReturnActivities(element)) {
                context.report(TF_COMMIT_ROLLBACK);
            }
        }
    }

    /**
     * This method parses the task flow file and checks for commit and rollback return activity instances.
     * Method returns 'false' if there is more than single occurance of either commit or rollback.
     * @param element
     * @return boolean
     */
    private boolean checkForReturnActivities(Element element) {
        int commitCounter = 0;
        int rollbackCounter = 0;
        Document document = element.getOwnerDocument();
        Node rootNode = document.getDocumentElement().getChildNodes().item(0);
        NodeList childNodesList = rootNode.getChildNodes();
        for (int nodes = 0; nodes < childNodesList.getLength(); nodes++) {
            Node node = childNodesList.item(nodes);
            if (commitCounter > 1 || rollbackCounter > 1) {
                return false;
            }
            if ("task-flow-return".equals(node.getNodeName()) &&
                "commit".equals(node.getLastChild().getLastChild().getNodeName())) {
                commitCounter++;
            }
            if ("task-flow-return".equals(node.getNodeName()) &&
                "rollback".equals(node.getLastChild().getLastChild().getNodeName())) {
                rollbackCounter++;
            }
        }
        return true;
    }
}
