package org.adfemg.audits.adfviewcontroller.datacontrol;

import oracle.jdeveloper.audit.analyzer.AuditContext;
import oracle.jdeveloper.audit.analyzer.Rule;
import oracle.jdeveloper.audit.extension.ExtensionResource;

import org.adfemg.audits.base.XmlAnalyzer;
import org.adfemg.audits.utils.AuditConstants;
import org.adfemg.audits.utils.XmlUtils;

import org.w3c.dom.Document;
import org.w3c.dom.Element;


/**
 * Analyzer class for DataControls.
 */
public class DataControlAnalyzer extends XmlAnalyzer {

    /**
     * [ADFcg1-03049] - No use of PlaceHolder data control - The placeholder data control is intended for
     * use during prototyping and mockups only.
     */
    @ExtensionResource("org.adfemg.audits.adfviewcontroller.datacontrol.place-holder-dc")
    private Rule PLACE_HOLDER_DC;

    public DataControlAnalyzer() {
        super(null);
    }

    @Override
    protected boolean isAuditable(Document document) {
        String rootElemName = XmlUtils.docElementName(document);
        return "DataControlConfigs".equals(rootElemName) || "AppModule".equals(rootElemName);
    }

    /**
     * Check if the AppModule has the placeholder NS.
     * Check if the PlaceholderDataControl element is found.
     */
    public void exit(AuditContext context, Element element) {
        if ("AppModule".equals(element.getNodeName())) {
            if (AuditConstants.NS_PLACEHOLDER_DC.equals(element.getAttribute("xmlns"))) {
                context.report(PLACE_HOLDER_DC);
            }
        }
        if ("PlaceholderDataControl".equals(element.getNodeName())) {
            context.report(PLACE_HOLDER_DC);
        }
    }
}
